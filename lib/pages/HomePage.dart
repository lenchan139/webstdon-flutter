import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:webston/Objects/InstanceInfo.dart';
import 'package:webston/Objects/MastodonAppInfo.dart';
import 'package:webston/Objects/SharedData.dart';
import 'package:webston/Utils/CommonUtils.dart';
import 'package:webston/Utils/MastodonApiHelper.dart';
import 'package:webston/Utils/SharedPreferencesManager.dart';
import 'package:webston/pages/WebViewPage.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _counter = 8;
  final instanceDomainInputController = TextEditingController();
  static const platform = const MethodChannel('app.channel.shared.data');
  Map<dynamic, dynamic> sharedData = Map();
  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text("Mastodon Instance Domain"),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              'Please enter your mastodon domain',
            ),
            Row(
              children: <Widget>[
                Text('https://'),
                Flexible(
                  child: TextFormField(
                    controller: instanceDomainInputController,
                    decoration:
                        InputDecoration(labelText: 'Mastodon Instance Domain'),
                  ),
                )
              ],
            ),
            FlatButton(
              child: Text("Enter"),
              onPressed: _onPressEnter,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  @override
  void initState() {
    super.initState();
    _initFromExternal();
    WidgetsBinding.instance.addPostFrameCallback((_) => {_onStartCheckPref()});
  }

  void _initFromExternal() async {
    // Case 1: App is already running in background:
    // Listen to lifecycle changes to subsequently call Java MethodHandler to check for shared data
    SystemChannels.lifecycle.setMessageHandler((msg) {
      if (msg.contains('resumed')) {
        _getSharedData().then((d) {
          if (d.isEmpty) return;
          // Your logic here
          // E.g. at this place you might want to use Navigator to launch a new page and pass the shared data
        });
      }
    });

    // Case 2: App is started by the intent:
    // Call Java MethodHandler on application start up to check for shared data
    var data = await _getSharedData();
    setState(() => sharedData = data);
    print("shareDataHome: ${sharedData}");
    // You can use sharedData in your build() method now
  }

  Future<Map> _getSharedData() async =>
      await platform.invokeMethod('getSharedData');

  @override
  void dispose() {
    instanceDomainInputController.dispose();
    super.dispose();
  }

  void _onStartCheckPref() async {
    print("starting check pref.");
    var pref = await SharedPreferences.getInstance();
    var domain = SharedPreferencesManager.getDomain(pref);
    var clientInfoJson = SharedPreferencesManager.getClientInfoJson(pref);
    var authToken = SharedPreferencesManager.getAuthToken(pref);
    try {
      if (domain != null &&
          domain.length > 0 &&
          clientInfoJson != null &&
          clientInfoJson.length > 0 &&
          authToken != null &&
          authToken.length > 0) {
        var clientInfo = MastodonAppInfo.fromJsonString(clientInfoJson);
        var redirectUrl = "https://${domain}";
        pushToWebViewPage(domain, clientInfo,
            preloadUrl: redirectUrl, authToken: authToken);
      } else if (domain != null &&
          domain.length > 0 &&
          clientInfoJson != null &&
          clientInfoJson.length > 0) {
        var clientInfo = MastodonAppInfo.fromJsonString(clientInfoJson);
        pushToWebViewPage(domain, clientInfo);
      }
    } catch (e) {
      print(e);
    }
  }

  void _onPressEnter() async {
    //CommonUtils.showToastBox(instanceDomainInputController.text);
    var domain = instanceDomainInputController.text;
    var urlPrefix = "https://${domain}";
    try {
      String testJson = await MastodonApiHelper.fetechInstanceInfo(urlPrefix);
      InstanceInfo instanceInfo = InstanceInfo.fromJsonString(testJson);
      print(instanceInfo.toString());
      if (instanceInfo.uri != null && instanceInfo.title != null) {
        var json = await MastodonApiHelper.registerClient(urlPrefix);
        var clientInfo = MastodonAppInfo.fromJsonString(json);
        print(clientInfo);
        if (clientInfo != null && clientInfo.clientId != null) {
          SharedPreferencesManager.setClientInfoJson(
              await SharedPreferences.getInstance(), json);
          pushToWebViewPage(domain, clientInfo);
        }
      } else {
        CommonUtils.showToastBox("Cannot connect this instance");
      }
    } catch (e) {
      print(e);
      CommonUtils.showToastBox(e.toString());
    }
  }

  void pushToWebViewPage(String domain, MastodonAppInfo mastodonAppInfo,
      {String preloadUrl, String authToken}) {
    var text = sharedData['text'];
    var subject = sharedData['subject'];
    SharedData sData;
    if (text != null || subject != null) sData = SharedData(text, subject);
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(
          builder: (ctx) => WebViewPage(
                domain,
                mastodonAppInfo: mastodonAppInfo,
                preloadUrl: preloadUrl,
                authToken: authToken,
                sharedData: sData,
              )),
    );
  }
}
