import 'package:http/http.dart' as http;
import 'package:webston/Objects/MastodonAppInfo.dart';
import 'dart:convert';
import 'package:webston/Utils/AppConst.dart';
class MastodonApiHelper{
  static Future<String> fetechInstanceInfo(String instanceDomain) async{
    var api_url = "${instanceDomain}/api/v1/instance";
    var response = await http.get(api_url);
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');
    return response.body;
  }
  static Future<String> registerClient(String instanceDomain) async{
    var api_url = "${instanceDomain}/api/v1/apps";
    var response = await http.post(api_url, body: {
      "client_name": AppConst.appName,
      "redirect_uris": "${AppConst.inAppAuthDomainHolder}/registerClient",
      "scopes": "read write follow push",
      "website": AppConst.appWebsite
    });
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');
    return response.body;
  }
}