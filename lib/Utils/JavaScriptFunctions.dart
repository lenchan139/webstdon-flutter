class JavaScriptFunctions {
  static String jsUpdateComposeForm(String newValue) {
    var content = newValue
        .replaceAll('\'', '\\\'')
        .replaceAll('\"', '\\\"')
        .replaceAll('`', "\\`");
    return '''
        document
        .getElementsByClassName('drawer')[0]
        .getElementsByClassName('compose-form')[0]
        .getElementsByTagName('textarea')[0]
        .value = `${content}`;
        
        ''';
  }

  static String jsFixKeyboard() {
    return '''
        inputs = document.querySelectorAll("input[type=text]");
        inputs.forEach(function(inp) {
            let finalInput = inp;
            finalInput.addEventListener("focus", function() {
                console.log('focus');
                input = finalInput;
                InputValue.postMessage('');
                Focus.postMessage('focus');
           });
           finalInput.addEventListener("focusout", function() {
               console.log('unfocus');
               Focus.postMessage('focusout');
           });
      });
''';
  }

  static String jsOnPageFinishEdFixKeyBoard() {
    return '''
                  window.input = null;
                  document.body.addEventListener('focus', (evt) => {
                    let element = evt.target;
                    if (element.tagName === 'INPUT' || element.tagName === 'TEXTAREA') {
                      window.input = element;
                      InputValue.postMessage(input.value);
                      Focus.postMessage('focus');
                    }
                  }, true);
                  document.body.addEventListener('click', (evt) => {
                    let element = evt.target;
                    if (window.input != null && element.tagName !== 'INPUT' && element.tagName !== 'TEXTAREA') {
                      window.input = null;
                      Focus.postMessage('focusout');
                    }
                  }, true);
                ''';
  }
}
