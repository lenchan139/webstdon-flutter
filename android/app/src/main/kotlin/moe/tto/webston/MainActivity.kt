package moe.tto.webston

import android.content.Intent
import android.os.Bundle
import io.flutter.app.FlutterActivity
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant


class MainActivity: FlutterActivity() {
  private val sharedData = HashMap<String, String>()
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    GeneratedPluginRegistrant.registerWith(this)

    // Handle intent when app is initially opened
    handleSendIntent(intent)
    println("andSharedDataType: ${intent.getType()}")
    println("andSharedDataAction: ${intent.getAction()}")
    println("andSharedDataContent: ${intent.getStringExtra(Intent.EXTRA_TEXT)}")

    MethodChannel(flutterView, "app.channel.shared.data").setMethodCallHandler { call, result ->
      if (call.method.contentEquals("getSharedData")) {
        result.success(sharedData)
        sharedData.clear()
      }
    }
  }

  override fun onNewIntent(intent: Intent?) {
    super.onNewIntent(intent)
    if(intent != null) handleSendIntent(intent)
  }

  private fun handleSendIntent(intent: Intent) {
    val action = intent.getAction()
    val type = intent.getType()

    // We only care about sharing intent that contain plain text
    if (Intent.ACTION_SEND.equals(action) && type != null) {
      if ("text/plain" == type) {
        sharedData["subject"] = intent.getStringExtra(Intent.EXTRA_SUBJECT)
        sharedData["text"] = intent.getStringExtra(Intent.EXTRA_TEXT)
        println("andSharedData: ${sharedData}");
      }
    }
  }
}
