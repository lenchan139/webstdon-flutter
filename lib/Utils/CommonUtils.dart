import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
class CommonUtils{
  static void showSnackBar(BuildContext context, String message) async{
    Scaffold.of(context).showSnackBar(new SnackBar(
      content: new Text(message),
    ));
  }
  static void showToastBox(String message) async{
    Fluttertoast.showToast(msg: message);
  }
}