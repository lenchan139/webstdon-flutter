import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webston/Objects/MastodonAppInfo.dart';
import 'package:webston/Objects/SharedData.dart';
import 'package:webston/Utils/AppConst.dart';
import 'package:webston/Utils/CommonUtils.dart';
import 'package:webston/Utils/JavaScriptFunctions.dart';
import 'package:webston/Utils/SharedPreferencesManager.dart';
import 'package:webston/pages/HomePage.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewPage extends StatefulWidget {
  WebViewPage(this.domain,
      {Key key,
      this.mastodonAppInfo,
      this.preloadUrl,
      this.authToken,
      this.sharedData})
      : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".
  String domain;
  String preloadUrl;
  String authToken;
  MastodonAppInfo mastodonAppInfo;
  SharedData sharedData;

  @override
  _WebViewPageState createState() => _WebViewPageState();
}

class _WebViewPageState extends State<WebViewPage> {
  static const platform = const MethodChannel('app.channel.shared.data');

  int _counter = 8;
  WebViewController webViewController;
  bool onStartChecker = true;
  bool shouldLoadSharedData = false;
  String initialUrl;
  String lastUrl = "";
  bool isLoggedOn = false;
  FocusNode _focusNode = FocusNode();
  @override
  Widget build(BuildContext context) {
    String oauthUrl =
        "https://${widget.domain}/oauth/authorize?response_type=code&client_id=${widget.mastodonAppInfo.clientId}&client_secret=${widget.mastodonAppInfo.clientSecret}&redirect_uri=${widget.mastodonAppInfo.redirectUri}&scope=read+push";
    initialUrl = oauthUrl;
    if (widget.authToken != null) {
      print("auth token: ${widget.authToken}");
      initialUrl = "https://${widget.domain}";
      isLoggedOn = true;
    }
    if (widget.preloadUrl != null) initialUrl = widget.preloadUrl;

    print("ttt1: ${widget.authToken}");
    print("ttt2: ${widget.sharedData}");
    print("ttt3: ${widget.sharedData.text ?? null}");
    print("ttt4: ${widget.sharedData.subject ?? null}");
    print(
        "ttt5: ${widget.authToken != null && widget.sharedData != null && (widget.sharedData.text != null || widget.sharedData.subject != null)}");
    if (widget.authToken != null &&
        widget.sharedData != null &&
        (widget.sharedData.text != null || widget.sharedData.subject != null)) {
      initialUrl = "https://${widget.domain}/web/statuses/new";
      shouldLoadSharedData = true;
    }
    print("initialUri: $initialUrl");

    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(AppConst.appName),
      ),
      body: RefreshIndicator(
        child: WebView(
          initialUrl: initialUrl,
          javascriptMode: JavascriptMode.unrestricted,
          onPageFinished: _onWebViewPageFinished,
          navigationDelegate: _navigateDecide,
          onWebViewCreated: (w) => {
            setState(() {
              webViewController = w;
            })
          },
        ),
        onRefresh: () {
          webViewController.reload();
        },
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _initFromExternal();
    WidgetsBinding.instance.addPostFrameCallback((_) => {_onStart()});
  }

  void _onStart() {
    print("onStart");
  }

  void _initFromExternal() async {
    // Case 1: App is already running in background:
    // Listen to lifecycle changes to subsequently call Java MethodHandler to check for shared data
    SystemChannels.lifecycle.setMessageHandler((msg) {
      print("lifecyle: ${msg}");
      if (msg.contains('resumed')) {
        _tryParseSharedData();
      }
    });

    // Case 2: App is started by the intent:
    // Call Java MethodHandler on application start up to check for shared data
    var data = await _getSharedData();
    var newData = SharedData(data["text"], data["subject"]);
    //setState(() => widget.sharedData = newData);
    //print("shareOnStartUp:" + data.toString());
    // You can use sharedData in your build() method now
  }

  void _tryParseSharedData() {
    _getSharedData().then((d) {
      if (d.isEmpty) return;
      // Your logic here
      // E.g. at this place you might want to use Navigator to launch a new page and pass the shared data
      print("sharedData: ${d}");
      shouldLoadSharedData = true;
      widget.sharedData = SharedData(d["text"], d["subject"]);
      onLoadComposeNew(d["subject"] + "\n" + d["text"]);
    });
  }

  Future<Map> _getSharedData() async =>
      await platform.invokeMethod('getSharedData');
  onLoadComposeNew(String content) {
    webViewController.loadUrl("https://${widget.domain}/web/statuses/new");
  }

  NavigationDecision _navigateDecide(NavigationRequest request) {
    var url = request.url;
    debugPrint("cap: ${url}");
    if (onStartChecker && url.contains('/timeline')) {
      onStartChecker = false;
    } else if (onStartChecker && url.contains('/about')) {
      onStartChecker = false;
      SharedPreferencesManager.clearAuthToken();
      pushToHomePage();
    } else if (url.contains('/auth/sign_out')) {
      // work on ios only.
      //logout();
    } else if (isLoggedOn && url.contains('/auth/sign_in')) {
      triggerSignInRoute();
    } else if (url.startsWith(AppConst.inAppAuthDomainHolder)) {
      print("oauth2redirect captured");
      print(url);
      String keyCode = "?code=";
      int indexCode = url.indexOf(keyCode);
      if (indexCode > 0) {
        var code = url.substring(indexCode + keyCode.length);
        int endIndexCode = code.indexOf("&");
        if (endIndexCode > 0) code = code.substring(0, endIndexCode);
        print(code);
        setPrefernces(widget.domain, widget.mastodonAppInfo.clientId,
            widget.mastodonAppInfo.clientSecret, code);
        if (webViewController != null)
          webViewController.loadUrl("https://${widget.domain}");
      } else {
        CommonUtils.showToastBox("Fetch Auth Token Failed");
      }
      return NavigationDecision.prevent;
    } else if (!url.startsWith("https://${widget.domain}")) {
      _openUrl(url);
      return NavigationDecision.prevent;
    }

    return NavigationDecision.navigate;
  }

  void setPrefernces(String domain, String clientId, String clientSecret,
      String authToken) async {
    var pref = await SharedPreferences.getInstance();
    await SharedPreferencesManager.setDomain(pref, domain);
    await SharedPreferencesManager.setAuthToken(pref, authToken);
    await SharedPreferencesManager.setClientId(pref, clientId);
    await SharedPreferencesManager.setClientSecret(pref, clientSecret);
  }

  void triggerSignInRoute() async {
    print("/auth/sign_in triggered");
    var pref = await SharedPreferences.getInstance();
    var authToken = SharedPreferencesManager.getAuthToken(pref);
    print("authToken: $authToken");
    if (authToken != null && authToken.length > 0) {
      logout();
      isLoggedOn = false;
    }
  }

  void logout() async {
    var pref = await SharedPreferences.getInstance();
    await SharedPreferencesManager.clearLoginInformation();
    await pushToHomePage();
  }

  void _openUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  void _onWebViewPageFinished(String uri) {
    print("onFinish uri: $uri");
    print("shouldLoad: ${shouldLoadSharedData}");
    print("urlComposeMiddle: ${uri.contains(AppConst.composeUrlMiddlefix)}");
    if (shouldLoadSharedData && uri.contains(AppConst.composeUrlMiddlefix)) {
      loadSharedDataToWeb();
    }
    webViewController.evaluateJavascript(JavaScriptFunctions.jsFixKeyboard());
  }

  void loadSharedDataToWeb() async {
    if (widget.sharedData != null) {
      var content = widget.sharedData.text;
      if (widget.sharedData.text != null &&
          widget.sharedData.subject != null &&
          widget.sharedData.text.length > 0 &&
          widget.sharedData.subject.length > 0) {
        content = "${widget.sharedData.subject}\n${widget.sharedData.text}";
      } else if (widget.sharedData.subject != null &&
          widget.sharedData.subject.length > 0) {
        content = widget.sharedData.subject;
      }
      // end content if
      var js = JavaScriptFunctions.jsUpdateComposeForm(content);
      print(js);
      var result = await webViewController.evaluateJavascript(js);
      print(result);
    }
  }

  void pushToHomePage() {
    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (ctx) => HomePage()));
  }
}
