
import 'dart:convert';

class MastodonAppInfo{
  String id;
  String name;
  String website;
  String redirectUri;
  String clientId;
  String clientSecret;
  String vapidKey;

  MastodonAppInfo({this.id, this.name, this.website, this.redirectUri, this.clientId, this.clientSecret, this.vapidKey});
  factory MastodonAppInfo.fromJson(Map<String, dynamic> json) => _infoFromJson(json);
  factory MastodonAppInfo.fromJsonString(String json) => _infoFromJsonString(json);

  @override
  String toString() {
    return "id=${id}, name=${name}, website=${website}, redirectUri=${redirectUri}, clientId=${clientId}, clientSecret=${clientSecret}, vapidKey=${vapidKey}";
  }
}

MastodonAppInfo _infoFromJsonString(String jsonString){
  var json = jsonDecode(jsonString);
  return MastodonAppInfo.fromJson(json);
}
MastodonAppInfo _infoFromJson(Map<String, dynamic> json) {
  return MastodonAppInfo(
      id: json['id'] as String,
      name: json['name'] as String,
      website: json['website'] as String,
      redirectUri: json['redirect_uri'] as String,
      clientId: json['client_id'] as String,
      clientSecret: json['client_secret'] as String,
      vapidKey: json['vapid_key'] as String
  );
}