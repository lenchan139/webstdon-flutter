import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesManager {
  static String _keyDomain = "key_domain";
  static String _keyClientId = "key_client_id";
  static String _keyClientSecret = "key_client_secret";
  static String _keyAuthToken = "key_auth_token";
  static String _keyClientInfoJson = "key_client_info_json";
  static String getDomain(SharedPreferences pref) {
    return pref.get(_keyDomain);
  }

  static void setDomain(SharedPreferences pref, String value) {
    pref.setString(_keyDomain, value);
  }

  static String getClientId(SharedPreferences pref) {
    return pref.getString(_keyClientId);
  }

  static void setClientId(SharedPreferences pref, String value) {
    pref.setString(_keyClientId, value);
  }

  static String getClientSecret(SharedPreferences pref) {
    return pref.getString(_keyClientSecret);
  }

  static void setClientSecret(SharedPreferences pref, String value) {
    pref.setString(_keyClientSecret, value);
  }

  static String getAuthToken(SharedPreferences pref) {
    return pref.getString(_keyAuthToken);
  }

  static String setAuthToken(SharedPreferences pref, String value) {
    pref.setString(_keyAuthToken, value);
  }

  static String getClientInfoJson(SharedPreferences pref) {
    return pref.get(_keyClientInfoJson);
  }

  static void setClientInfoJson(SharedPreferences pref, String value) {
    pref.setString(_keyClientInfoJson, value);
  }

  static void clearLoginInformation() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    await pref.remove(_keyAuthToken);
    await pref.remove(_keyClientId);
    await pref.remove(_keyClientInfoJson);
    await pref.remove(_keyClientSecret);
    await pref.remove(_keyDomain);
  }

  static void clearAuthToken() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    await pref.remove(_keyAuthToken);
  }
}
