
import 'dart:convert';

class InstanceInfo{
  String title;
  String uri;
  String description;
  String email;
  String version;
  String createAt;
  bool registrations;

  InstanceInfo({this.title, this.uri, this.description, this.email, this.version, this.createAt, this.registrations});
  factory InstanceInfo.fromJson(Map<String, dynamic> json) => _infoFromJson(json);
  factory InstanceInfo.fromJsonString(String json) => _infoFromJsonString(json);

  @override
  String toString() {
    return "title=${title}, uri=${uri}, description=${description}, email=${email}, version=${version}, createAt=${createAt}, registrations=${registrations}";
  }
}

InstanceInfo _infoFromJsonString(String jsonString){
  var json = jsonDecode(jsonString);
  return InstanceInfo.fromJson(json);
}
InstanceInfo _infoFromJson(Map<String, dynamic> json) {
  return InstanceInfo(
      title: json['title'] as String,
      uri: json['uri'] as String,
      description: json['description'] as String,
      email: json['email'] as String,
      version: json['version'] as String,
      createAt: json['create_at'] as String,
      registrations: json["registrations"] as bool
  );
}